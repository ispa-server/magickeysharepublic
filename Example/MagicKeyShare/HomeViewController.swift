//
//  ViewController.swift
//  MagicKeyShare
//
//  Created by James Trigg on 05/17/2021.
//  Copyright (c) 2021 James Trigg. All rights reserved.
//

import UIKit
import FCAlertView
import MagicKeyShare

let splitVC = UISplitViewController(style: .doubleColumn)

extension UIViewController : MenuControllerDelegate {
    func didTapMenuItem(at index: Int, title: String?) {

        switch (index) {
            case 1:
            let vc = MainStoryboard.instantiateViewController(withIdentifier: "KeychainGroupViewController")
            (splitVC.viewControllers.last as? UINavigationController)?.pushViewController(vc, animated: true)
                break;
            default:
                let vc = UIViewController()
                vc.view.backgroundColor = .systemRed
                vc.title = title
            (splitVC.viewControllers.last as? UINavigationController)?.pushViewController(vc, animated: true)
        }

    }
}

class HomeViewController: UIViewController {
    private var keychain : MagicKeyShare? = nil
    private var ready : Bool = false
    let keyName = "MyKey"
    private var somethingToSave = "random string"

    override func viewDidLoad() {
        super.viewDidLoad()
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 300, height: 70))
        button.setTitle("Show Examples", for: .normal)
        button.backgroundColor = .systemBlue
        view.addSubview(button)
        button.center = view.center
        button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)

    }

    @objc private func didTapButton() {
        let menuVC = MenuViewController(style: .plain)
        menuVC.delegate = self

        let secondVC = UIViewController()

        secondVC.view.backgroundColor = .systemBlue
        secondVC.title = "Home"

        splitVC.viewControllers = [
            UINavigationController(rootViewController: menuVC),
            UINavigationController(rootViewController: secondVC)
        ]


        present(splitVC, animated: true)

    }
}
