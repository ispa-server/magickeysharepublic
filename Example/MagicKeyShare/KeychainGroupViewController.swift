//
//  KeyChainViewController.swift
//  MagicKeyShare_Example
//
//  Created by James Trigg on 28/03/2022.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import UIKit
import FCAlertView
import MagicKeyShare

class KeychainGroupViewController: UIViewController {
    
    private var keychain : MagicKeyShare? = nil
    private var ready : Bool = false
    let keyName = "MyKey"
    private var somethingToSave = "random string"
    @IBOutlet weak var keychainGroupName: UILabel!
    @IBOutlet weak var savedToKeychainLabel: UILabel!
    @IBOutlet weak var loadedFromKeychainLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        do {
            keychain = try MagicKeyShare.shared()
            keychainGroupName.text = keychain!.keychainGroupName!
            ready = true
        } catch let error {
            self.showError(
                withTitle: "MagicKeyShare init Failed",
                withSubtitle: error.localizedDescription
            )
        }
    }
    
    @IBAction func saveToKeychain(_ sender: UIButton) {
        if(!ready) {
            notReady()
            return
        }
        somethingToSave = randomString(length: 16)
        showAlert(
            withTitle: "Saving Data",
            withSubtitle: "Saving \"\(somethingToSave)\" to the keychain group"
        )
        if(
            keychain!.addKeyValueToKeyChain(
                keyName: keyName,
                value: somethingToSave
            )
        ) {
            savedToKeychainLabel.text = "Saved: \(somethingToSave)"

        } else {
            savedToKeychainLabel.text = "Saved failed"
        }
        
    }
    
    @IBAction func loadFromKeychain(_ sender: UIButton) {
        if(!ready) {
            notReady()
            return
        }
        let savedData = keychain!.returnKeyFromKeychain(keyName: keyName)
        showAlert(
            withTitle: "Load Data",
            withSubtitle: "Loaded \"\(savedData)\" from the keychain group"
        )
        loadedFromKeychainLabel.text = "Loaded : \(savedData)"
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func notReady() {
        self.showError(
            withTitle: "View is not Ready!",
            withSubtitle: "The view may not have been setup correctly."
        )
    }
    
    func randomString(length: Int) -> String {
      let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
      return String((0..<length).map{ _ in letters.randomElement()! })
    }
}





