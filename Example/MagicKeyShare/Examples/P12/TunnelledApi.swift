//
//  TunnelledApi.swift
//  MagicKeyShare_P12
//
//  Created by James Trigg on 06/05/2022.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Foundation
import IDPClientSDK
import MagicKeyShare

public enum TunnelledApiError: Error {
    case initFail(reason : String)
}

struct IPAddress : Decodable {
    var ip : String
}

struct TunnelledApi {
    let session : URLSession
    let kc : P12MtlsHelper
    
    init() throws {
        // get p12 from key chain
        kc = try P12MtlsHelper.shared()
        let status = kc.getP12FromKeychainGroup()
        if(status != "OK") {
            throw TunnelledApiError.initFail(reason: "keyChain error \(status)")
        }
        
        // prepare credentials for tunnel
        let credential = URLCredential(identity: self.kc.sidentity1!, certificates: nil, persistence: .forSession)
        
        // setup tunnel
        IDPNetwork.setMTLSProxy(host: "isag.iden319.net", port: 3128, identity: credential.identity)
        
        let sessionConfig = URLSessionConfiguration.default as URLSessionConfiguration
        IDPLogger.info("Session Created \(sessionConfig)")
        
        // Add CA certs so the connection can be trusted
        IDPNetwork.singleton().RefreshCertificateStoreFromKeyChain()
        IDPNetwork.singleton().ConfigSessionConfiguration(sessionConfiguration: sessionConfig)
        IDPLogger.info("Session Configured \(sessionConfig)")
        
        // Our Tunnelling session is now set up
        session = URLSession.init(
            configuration: sessionConfig
        )
    }

    func getMyIP() async throws -> IPAddress {
        let url = URL(string: "https://api.ipify.org?format=json")!
        let (data, _) = try await session.data(from: url)
        let decoder = JSONDecoder()
        return try decoder.decode(IPAddress.self, from: data)
    }
}
