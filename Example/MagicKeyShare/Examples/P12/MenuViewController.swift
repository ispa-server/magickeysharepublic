import UIKit
import SwiftUI
import MagicKeyShare
import IDPClientSDK

extension Notification.Name {
    static let didReceiveData = Notification.Name("didReceiveData")
    static let didCompleteTask = Notification.Name("didCompleteTask")
    static let completedLengthyDownload = Notification.Name("completedLengthyDownload")
}

class MenuViewController: UIViewController {
    @IBOutlet weak var unauthenticatedMtlsButton: UIButton!
    @IBOutlet weak var authenticateButton: UIButton!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var expires: UILabel!
    @IBOutlet weak var deletep12Button: UIButton!
    private var api : TunnelledApi? = nil
    private var p12Helper : P12MtlsHelper? = nil
    @State private var showOptionModal = false
    let AuthUrlDefault = "https://jltmacmini.local/idp"
    
    override func viewDidLoad() {
        self.navigationItem.title = "idenprotect MTLS Demo"
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .didReceiveData, object: nil)
        
    }

    override func viewDidAppear(_ animated: Bool) {
        do {
            p12Helper = try P12MtlsHelper.shared()
            let status = p12Helper!.getP12FromKeychainGroup()
            if(status != "OK") {
                print("Could not get P12 from Keychain : \(status)")
                return
            }
            Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
            
            super.viewDidAppear(animated)
            let defaults = UserDefaults.standard
            let authUrl : String? = defaults.string(forKey: "authUrl")
            if(authUrl == nil) {
                showOptions()
            }
        } catch let error {
            self.showError(
                withTitle: "P12MtlsHelper init Faild",
                withSubtitle: error.localizedDescription
            )
        }
    }
    
    @objc func updateTime() {
        let timeLeft = p12Helper!.getUserCertTimeToExpire()
        if(timeLeft == "unknown") {
            authenticateButton.setTitle("Get p12", for: .normal)
        } else {
            authenticateButton.setTitle("Renew p12", for: .normal)
        }
        expires.text = "p12 Expires In : \(timeLeft)"
    }
    
    @IBAction func useTestP12ButtonPressed(_ sender: Any) {
        if(!p12Helper!.loadDemoP12intoKeyChain(demoP12: demoP12, demoP12ExpireDate: demoP12ExpireDate)) {
            IDPLogger.error("Cound not add demo p12 to keychain")
        }
    }
    
    @IBAction func onAuthButtonPressed(_ sender: Any) {
        mainLabel.text = "Renewing certificate";
        let idenProtectHook = "idenprotect://callback/p12renew/mtlsdemo";
        guard let idenUrl = URL(string: idenProtectHook) else {
            showAlert(withTitle: "Open App Error", withSubtitle: "Could not open application")
            return
        }
        if (UIApplication.shared.canOpenURL(idenUrl)) {
            UIApplication.shared.open(idenUrl) { outcome in
                IDPLogger.info("Tryed to open Url : \(idenUrl) with \(outcome ? "Success" : "Fail")")
            }
        }
    }
    
    @IBAction func deleteP12ButtonPressed(_ sender: Any) {
        if(p12Helper!.deleteP12()) {
            showAlert(withTitle: "p12 Deleted", withSubtitle: "p12 was removed from the shared keychain")
        } else {
            showError(withTitle: "Unable to deleted p12", withSubtitle: "Error removing p12 from the shared keychain")
        }
    }
    
    @IBAction func onSwiftMtlsButtonPress(_ sender: Any) {
        let newViewController = self.storyboard?.instantiateViewController(withIdentifier: "webViewController") as! MutualTlsViewController
        self.navigationController?.pushViewController(newViewController, animated: false)
    }
    
    @IBAction func onObjCMtlsButtonPress(_ sender: Any) {
        let newViewController = self.storyboard?.instantiateViewController(withIdentifier: "ObjCWebViewController") as! ObjCWebViewController
        self.navigationController?.pushViewController(newViewController, animated: false)
    }
    
    @IBAction func onTunnelButtonPress(_ sender: Any) {
        let newViewController = self.storyboard?.instantiateViewController(withIdentifier: "TunnelViewController") as! TunnelViewController
        self.navigationController?.pushViewController(newViewController, animated: false)
    }
    
    @IBAction func optionsButtonPressed(_ sender: Any) {
        showOptions()
    }
    
    @IBAction func whatsMyIpApiCallButtonPressed(_ sender: Any) {
        Task {
            await whatsMyIpApiCall()
        }
    }
    
    func whatsMyIpApiCall() async {
        do {
            if(api == nil) {
                startSpinner()
                api = try TunnelledApi.init()
            }
            let myIP = try await api!.getMyIP()
            stopSpinner()
            showAlert(withTitle: "Your IP", withSubtitle: myIP.ip)
        } catch {
            stopSpinner()
            showError(withTitle: "Failed to get your IP address", withSubtitle: error.localizedDescription)
        }
    }
    
    func showCertificateDetails() {
        do {
            _ = try p12Helper!.getMainCert()
            let subject = (p12Helper!.scert != nil) ? "Found" : "Not Found"
            let expiry = "\(p12Helper!.scert.hashValue)"
                //keychainAccess.getCertInfo()
            
            let bridge = ViewModel()
            let vc = UIHostingController(
                rootView: CertificateDetailsView(
                    vm: bridge,
                    subject : subject,
                    expiry : expiry
                )
            )
            bridge.closeAction = { [weak vc] in
                vc?.dismiss(animated: true)
            }
            self.present(vc, animated: true, completion: nil)
        } catch {
            // Create a new alert
            showAlert(withTitle: "Error", withSubtitle: "\(error)")
        }
    }
    
    func showOptions() {
        let defaults = UserDefaults.standard
        let authUrl : String = defaults.string(forKey: "authUrl") ?? AuthUrlDefault
        let bridge = ViewModel()
        let vc = UIHostingController(
            rootView: OptionsView(
                vm: bridge,
                authUrl: authUrl
            )
        )
        bridge.closeAction = { [weak vc] in
            vc?.dismiss(animated: true)
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    func loadScreen() {
        let newViewController = self.storyboard?.instantiateViewController(withIdentifier: "webViewController") as! MutualTlsViewController
        self.navigationController?.pushViewController(newViewController, animated: false)
    }
    
    @objc func onDidReceiveData(_ notification:Notification) {
        mainLabel.text = "Authenticated"
        loadScreen()
    }
}

