//
//  TunnelViewController.swift
//  MagicKeyShare_P12
//
//  Created by James Trigg on 03/05/2022.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Foundation
import UIKit
import IDPClientSDK
import MagicKeyShare

struct Message {
    let title : String
    let message : String
}

class TunnelViewController: MutualTlsViewController {
    private var kc : P12MtlsHelper? = nil
    private var ready : Bool = false
    internal var error : Message?
    
    override func loadView() {
        do {
            kc = try P12MtlsHelper.shared()
            let status = kc!.getP12FromKeychainGroup()
            if(status != "OK") {
                self.error = Message(
                    title: "Could not get P12 from Keychain",
                    message: status
                )
                super.loadView()
                return
            }
            ready = true
            let credential = URLCredential(identity: self.kc!.sidentity1!, certificates: nil, persistence: .forSession)
            IDPNetwork.setMTLSProxy(host: "isag.iden319.net", port: 3128, identity: credential.identity)
            configuration = WKWebViewConfiguration()
            
            IDPNetwork.singleton().SetWKWebViewConfiguration(configuration: configuration!)
            IDPNetwork.singleton().RefreshCertificateStoreFromKeyChain()
            
        } catch let error {
            self.error = Message(
                title: "P12MtlsHelper init Faild",
                message: error.localizedDescription
            )
        }
        super.loadView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(self.error != nil) {
            ShowError(
                title: error!.title,
                message: error!.message
            )
            return
        }
        loadWebview()
    }
    
    override func loadWebview() {
        let defaults = UserDefaults.standard
        url = URL(string:
            defaults.string(forKey: "tunnelUrl") ??
                  "https://ap001.ad.iden319.net/testapp/testapp.htm"
            )!
        if((url?.absoluteString) != nil) {
            IDPLogger.info("URL = \(String(describing: url?.absoluteString))")
        } else {
            IDPLogger.error("URL Not Found")
        }
        
        navigationController?.navigationBar.barTintColor = UIColor(named: "checking")
        super.loadWebview()
    }
    
    func urlSession(
        _ session: URLSession,
        task: URLSessionTask,
        didReceive challenge: URLAuthenticationChallenge,
        completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void
    ) {
        ShowError(title : "URLAuthenticationChallenge", message : "Challenge: \(challenge.protectionSpace.authenticationMethod)")
        completionHandler(URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil)
    }
    
}
