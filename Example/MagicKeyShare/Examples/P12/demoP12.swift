//
//  demoP12.swift
//  MagicKeyShare_P12
//
//  Created by James Trigg on 06/05/2022.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Foundation

public let demoP12ExpireDate = "2023-04-21T15:28:45+0100"
public let demoP12 = """
Ask Idenprotect for your test P12
"""
