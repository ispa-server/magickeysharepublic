import Foundation
import UIKit
import WebKit
import CoreFoundation
import MagicKeyShare
import IDPClientSDK

enum webViewError: Error {
    case OSStatus(OSStatus : Int32)
    case missingCertificate
    case authTypeMismatch(expectedType : String)
    case kSecImportItemCertChainNotFound
    case kSecImportItemTrustNotFound
}

class WebViewController: MutualTlsViewController {
    private var kc : P12MtlsHelper? = nil
    private var ready : Bool = false

    
    override func viewDidAppear(_ animated: Bool) {
        do {
            kc = try P12MtlsHelper.shared()
            let status = kc!.getP12FromKeychainGroup()
            if(status != "OK") {
                IDPLogger.error("Could not get P12 from Keychain: \(status)")
                ShowError(
                    title: "Could not get P12 from Keychain",
                    message: status
                )
                return
            }
            ready = true
            loadWebview()
        } catch let error {
            IDPLogger.error("P12MtlsHelper init Faild: \(error.localizedDescription)")
            ShowError(
                title: "P12MtlsHelper init Faild",
                message: error.localizedDescription
            )
        }
    }
    
    override func loadWebview() {
        let defaults = UserDefaults.standard
        url = URL(string:
            defaults.string(forKey: "authUrl") ??
            "https://jltmacmini.local/idp"
            )!
        if(url?.absoluteString == nil) {
            IDPLogger.error("Cound not create URL from String")
            return
        } else {
            IDPLogger.info("URL = \(url!.absoluteString)")
        }
        navigationController?.navigationBar.barTintColor = UIColor(named: "checking")
        super.loadWebview()
    }
    
    func urlSession(
        _ session: URLSession,
        task: URLSessionTask,
        didReceive challenge: URLAuthenticationChallenge,
        completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void
    ) {
        ShowError(title : "URLAuthenticationChallenge", message : "Challenge: \(challenge.protectionSpace.authenticationMethod)")
        completionHandler(URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil)
    }
}
