//
//  ObjCWebViewController.h
//  MagicKeyShare_P12
//
//  Created by James Trigg on 04/04/2022.
//  Copyright © 2022 CocoaPods. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "MagicKeyShare-Swift.h"

typedef NS_ENUM(NSInteger, WebViewError) {
    MissingCertificate,
    AuthTypeMismatch,
    kSecImportItemCertChainNotFound,
    kSecImportItemTrustNotFound
};

@interface ObjCWebViewController : UIViewController
@property (nonatomic, assign, readwrite) WebViewError webViewError;
@end
