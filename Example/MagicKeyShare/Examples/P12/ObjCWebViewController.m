//
//  ObjCWebWiewController.m
//  MagicKeyShare
//
//  Created by James Trigg on 29/03/2022.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ObjCWebViewController.h"
#import <IDPClientSDK/IDPClientSDK-Swift.h>


@interface ObjCWebViewController ()
@property (weak, nonatomic) IBOutlet WKWebView *webView;
@property (weak, nonatomic) NSObjectP12MtlsHelper* p12Helper;
@property BOOL ready;

@end

@implementation ObjCWebViewController
    

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.p12Helper = [NSObjectP12MtlsHelper shared];
    self.ready = NO;
    NSString* status = [self.p12Helper getP12FromKeychainGroup];
    if(![status isEqualToString:@"OK"]) {
        [self showAlertWithTitle:@"Could not get P12 from Keychain" withSubtitle:status doneButtonAction: nil];
        return;
    }
    [self loadWebview];
}

- (void)loadWebview {
    NSUserDefaults* defaults = NSUserDefaults.standardUserDefaults;
    NSURL* url = [NSURL URLWithString:[defaults valueForKey:@"authUrl"]];
    if(url == nil) {
        NSLog(@"URL Error : %@", [url absoluteString]);
        [self showAlertWithTitle:@"URL Error" withSubtitle:[NSString stringWithFormat:@"options URL : \"%@\" was not valid",[url absoluteString]] doneButtonAction: nil];
        return;
    }
    NSLog(@"URL = %@", [url absoluteString]);
    NSURLRequest* request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
}

- (void)URLSession:(NSURLSession *)session
didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
 completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler {
    @try {
        if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
        {
 //           if([challenge.protectionSpace.host isEqualToString:@"DomainNameOfServer.io"]){
                NSURLCredential * credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
                completionHandler(NSURLSessionAuthChallengeUseCredential,credential);
 //           }
            return;
        }
        if(![challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodClientCertificate])
        {
            NSException* exception = [NSException
                    exceptionWithName:@"WebViewErrorAuthTypeMismatch"
                    reason:challenge.protectionSpace.authenticationMethod
                    userInfo:nil];
            @throw exception;
        }
        // Certificate Auth

        NSString* status = [self.p12Helper getMainCert];
        if(![status isEqualToString:@"errSecSuccess"]) {
            NSException* exception = [NSException
                    exceptionWithName:@"MainCertError"
                    reason:status
                    userInfo:nil];
            @throw exception;
        }
        
        NSURLCredential * credential = [NSURLCredential credentialWithIdentity:[self.p12Helper getSidentity1] certificates:nil persistence:NSURLCredentialPersistenceForSession];
        completionHandler(NSURLSessionAuthChallengeUseCredential,credential);
        
    }
    @catch(NSException *e) {
        [self showAlertWithTitle:e.name withSubtitle:e.description doneButtonAction: nil];
    }
}
@end
