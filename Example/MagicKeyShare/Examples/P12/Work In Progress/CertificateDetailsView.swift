import UIKit
import SwiftUI

struct CertificateDetailsView: View {
    
    var vm: ViewModel
    let subject : String
    let expiry : String
    
    var body: some View {
        VStack {
            Text("Certificate Details")
                .font(.system(size: 60))
                .padding(20)
            Spacer()
            Text("Certificate : \(subject)")
                .font(.system(size: 30))
                .padding(20)
            Text("Hash : \(expiry)")
                .font(.system(size: 20))
                .padding(20)
                
            HStack {
                Spacer()
                Button("Close") {
                    self.vm.closeAction()
                }.font(.system(size: 30))
                Spacer()
            }.padding(20)
            Spacer()
            Spacer()
            Spacer()
 
        }.padding(20)
        
    }
}

struct CertificateDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        CertificateDetailsView(
            vm: ViewModel(),
            subject: "some Subject",
            expiry: "Sept 16th 2020"
        )
    }
}
