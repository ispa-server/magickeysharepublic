import UIKit
import SwiftUI

class ViewModel {
    var closeAction: () -> Void = {}
}

extension String {
    func validateUrl () -> Bool {
        let urlRegEx = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
        return NSPredicate(format: "SELF MATCHES %@", urlRegEx).evaluate(with: self)
    }

    func verifyUrl()-> Bool {
        if let url = NSURL(string: self) {
            return UIApplication.shared.canOpenURL(url as URL)
        }
        return false
    }
}

// not used at moment as Steve at UBS is having issue with it


struct OptionsView: View {
    
    let defaults = UserDefaults.standard
    //First get the nsObject by defining as an optional anyObject
    let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
    
    var vm: ViewModel
    @State var authUrl : String
    @State var validUrl : Bool = true
    
    
    
    var body: some View {
        VStack {
            Text("Options")
                .font(.system(size: 60))
                .padding(20)
            Text("v\(version)")
            Spacer()
            Text("Please enter the URL of the server you want to authenticate with.")
            TextField(
                "url...",
                text: $authUrl
            ).disableAutocorrection(true)
             .autocapitalization(.none)
             .keyboardType(.URL)
             .border(Color.accentColor)
             .textFieldStyle(RoundedBorderTextFieldStyle())
             .font(.system(size: 30))
            if(!"https://google.com".verifyUrl()) {
                Text("Permission to use Safari maybe disabled!")
                    .font(.system(size: 30))
                    .foregroundColor(Color.red)
            } else if(!self.validUrl) {
                Text("A valid URL is required!")
                    .font(.system(size: 30))
                    .foregroundColor(Color.red)
            } else {
                Text("")
                .font(.system(size: 30))
            }
            HStack {
                Spacer()
                Button("Close") {
                    self.vm.closeAction()
                }.font(.system(size: 30))
                Spacer()
                Button("Save") {
                    if(self.authUrl.verifyUrl()) {
                        self.defaults.set(self.authUrl, forKey: "authUrl")
                        self.vm.closeAction()
                    } else {
                        self.validUrl = false
                    }
                }.font(.system(size: 30))
                Spacer()
            }.padding(20)
            Spacer()
            Spacer()
            Spacer()
 
        }.padding(20)
        
    }
}

struct OptionsView_Previews: PreviewProvider {
    static var previews: some View {
        OptionsView(vm: ViewModel(), authUrl: "test.com")
    }
}
