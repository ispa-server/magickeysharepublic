//
//  P12ViewController.swift
//  MagicKeyShare_Example
//
//  Created by James Trigg on 03/06/2021.
//  Copyright © 2021 CocoaPods. All rights reserved.
//

import UIKit
import FCAlertView
import MagicKeyShare

class P12ViewController: UIViewController {
    private var p12Helper : P12MtlsHelper? = nil
    private var ready : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        do {
            p12Helper = try P12MtlsHelper.shared()
            let status = try p12Helper!.getP12FromKeychainGroup()
            if(status != "OK") {
                self.showError(
                    withTitle: "Could not get P12 from Keychain",
                    withSubtitle: status
                )
                return
            }
            ready = true
        } catch let error {
            self.showError(
                withTitle: "P12MtlsHelper init Failed",
                withSubtitle: error.localizedDescription
            )
        }
    }
}
