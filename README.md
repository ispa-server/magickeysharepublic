# MagicKeyShare

## Example
To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

iOS 13+

## Installation

### You may need to update you ruby version
\curl -sSL https://get.rvm.io | bash -s stable --ruby

### You may need to install cocoapods
 sudo gem install cocoapods

added the following line to your podfile
```ruby
    pod 'MagicKeyShare', :git => 'https://bitbucket.org/ispa-server/magickeysharepublic.git'
```

## How to use Keychain Sharing
add to the top of each file that requires it

import MagicKeyShare

When you need to create/renew the p12 you will need to link to the idenprotect for intune app

let idenProtectHook = "idenprotect://callback/p12renew/[url app name]";
        let idenUrl = NSURL(string: idenProtectHook);
        if (UIApplication.shared.canOpenURL(idenUrl! as URL)) {
            UIApplication.shared.openURL(idenUrl! as URL)
        }

Our demo app uses mtlsdemo
So for us it looks like this
let idenProtectHook = "idenprotect://callback/p12renew/mtlsdemo";
You will need to added your own
let idenProtectHook = "idenprotect://callback/p12renew/yourappname";

Also make sure you have idenprotect in your LSApplicationQueriesSchemes in your apps plist

### UIKit

The easiest way to integrate is to extend __MutualTlsViewController__

You can use P12MtlsHelper public func's to help you manage the p12 from the Comfort of
your app. Uses the Singelton pattern and you can easily use the shared instance by
calling __P12MtlsHelper.shared()__

__getUserCertRenewalDate()__ will return the date the p12 will need to be renewed by.

__deleteP12()__ deletes the p12 from the keychain and returns true if successful.

__willHaveExpiredIn(seconds)__ is useful to ask the question will p12 have expired in a number of seconds

__getUserCertTimeToExpire()__ Great for showing a message to your users about how long they have to work with the P12- returns "5 Days", "4h 3m 3s", "Expired" or "Unknown"

# IDPClientSDK

IDPClientSDK is our latest SDK that allows you to create MTLS tunnels amongst other things.

## Examples for using MTLS tunnels
See the TunnelViewController for an example of using it with a webview
and TunnelledApi for an URLSession Example

## Running the example app
Select the MagicKeyShare_P12 target
Added your developer details to signing and capabilities tap.
Ask idenprotect for your demo P12 and place it in the demoP12 file.
Now attach and select an iOS device and Press the play button in Xcode
When the app is running you will be presented with several options.
The tunnelling options are the bottom to on this page.
One of the two options as an example of a tunnelling webview and one of a API call

## Author

James Trigg, james.trigg@idenprotect.com

## License

MagicKeyShare is available under license.

https://www.idenprotect.com/eula/
