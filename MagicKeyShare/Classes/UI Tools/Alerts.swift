//
//  Alerts.swift
//  MagicKeyShare_Example
//
//  Created by James Trigg on 02/06/2021.
//  Copyright © 2021 CocoaPods. All rights reserved.
//

import Foundation
import FCAlertView

extension UIViewController {
    @objc public func showAlert(
        withTitle title: String ,
        withSubtitle subtitle: String,
        doneButtonAction : FCActionBlock? = nil
    ) {
        let alert = FCAlertView()
        if (self.traitCollection.userInterfaceStyle == .dark) {
            alert.darkTheme = true
        }
        alert.showAlert(
            inView: self,
            withTitle: title,
            withSubtitle: subtitle,
            withCustomImage: nil,
            withDoneButtonTitle: nil,
            andButtons: nil
        )
        if(doneButtonAction != nil) {
            alert.doneBlock = doneButtonAction
        }
    }
    
    @objc public func showYesNoAlert(
        withTitle title: String ,
        withSubtitle subtitle: String,
        yesButtonAction : @escaping FCActionBlock,
        noButtonAction : @escaping FCActionBlock
    ) {
        let alert = FCAlertView()
        if (self.traitCollection.userInterfaceStyle == .dark) {
            alert.darkTheme = true
        }
        
        alert.addButton("Yes", withActionBlock : yesButtonAction )
        
        alert.doneBlock = noButtonAction
        
        alert.showAlert(
            inView: self,
            withTitle: title,
            withSubtitle: subtitle,
            withCustomImage: nil,
            withDoneButtonTitle: "No",
            andButtons: nil
        )
    }

    @objc public func showError(
        withTitle title: String ,
        withSubtitle subtitle: String,
        doneButtonAction : FCActionBlock? = nil
    ) {
        let alert = FCAlertView()
        if (self.traitCollection.userInterfaceStyle == .dark) {
            alert.darkTheme = true
        }
        alert.makeAlertTypeWarning()
        alert.showAlert(
            inView: self,
            withTitle: title,
            withSubtitle: subtitle,
            withCustomImage: nil,
            withDoneButtonTitle: nil,
            andButtons: nil
        )
        if(doneButtonAction != nil) {
            alert.doneBlock = doneButtonAction
        }
    }
}
