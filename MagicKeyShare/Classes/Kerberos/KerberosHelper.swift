import Foundation

public enum KerberosHelperError : Error{
    case initFail(withError : Error)
}

extension URLRequest {
    mutating func setKerberosHeader(kerberosToken : String) {
        self.addValue (
            "Negotiate \(kerberosToken)",
            forHTTPHeaderField: "Authorization"
        )
    }
}

public class KerberosHelper {
    private let keyChain: MagicKeyShare
    private static var share : KerberosHelper?
    private let currentHostName : String = "testkerb.idenprotect.net"
    
    init() throws {
        do {
            keyChain = try MagicKeyShare.shared()
        } catch let MagicKeyShareError.initFail(withError: error) {
            throw KerberosHelperError.initFail(withError : error)
        }
    }
    
    class public func shared() throws -> KerberosHelper!  {
        if(KerberosHelper.share == nil) {
            do {
                KerberosHelper.share = try KerberosHelper()
            } catch let error {
                throw KerberosHelperError.initFail(withError : error)
            }
        }
        return KerberosHelper.share!
    }
    
    //Call the SDK to get the generated token
    public func getKerberosToken (hostName : String) -> String {
        
        return "TEMP"
    }
    
    
    public func checkIfKerberosProtectedSite (url: String) {
        //Call HTTPClient function to check if Kerberos URL is protected
        //Use the policy functionality i.e. whitelist as discussed earlier in call
        //Via a local DB pulling down the policy in some way
    }
}
