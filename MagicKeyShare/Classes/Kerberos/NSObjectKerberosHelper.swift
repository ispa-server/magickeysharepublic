import Foundation

@objc public class NSObjectKerberosHelper : NSObject{
    private static let share : NSObjectKerberosHelper = NSObjectKerberosHelper()
    var kerberosHelper : KerberosHelper?
    
    @objc class public func shared() -> NSObjectKerberosHelper {
        return NSObjectKerberosHelper.share
    }
    
    override init() {
        do {
            self.kerberosHelper = try KerberosHelper.shared()
        } catch let error {
            print(error)
        }
        super.init()
    }
    
}
