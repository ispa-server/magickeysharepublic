import Foundation

@objc public class NSObjectMagicKeyShare : NSObject{
    private static let share : NSObjectMagicKeyShare = NSObjectMagicKeyShare()
    var magicKeyShare : MagicKeyShare?
    
    @objc class public func shared() -> NSObjectMagicKeyShare {
        return NSObjectMagicKeyShare.share
    }
    
    override init() {
        do {
            self.magicKeyShare = try MagicKeyShare.shared()
        } catch let error {
            print(error)
        }
        super.init()
    }
    
    @objc public func addKeyValueToKeyChain(withKeyName keyName : String, value: String) -> Bool {
        if(magicKeyShare == nil) {
            return false
        }
        return magicKeyShare!.addKeyValueToKeyChain(keyName: keyName, value: value);
    }
    
    @objc public func deleteKeyFromKeychain(keyName: String) -> Bool {
        if(magicKeyShare == nil) {
            return false
        }
        return magicKeyShare!.deleteKeyFromKeychain(keyName: keyName);
    }
}
