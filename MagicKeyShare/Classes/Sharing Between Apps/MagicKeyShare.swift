import Foundation
import SwiftKeychainWrapper
import SwiftEntitlements
import IDPClientSDK

public enum MagicKeyShareError: Error {
    case initFail(withError : Error)
    case NoKeychainGroupFound
    case getSharedKeychainStringFailed
    case identityDictionariesNotFoundInP12
    case sidentity1NotFound
    case kSecImportItemCertChainNotFound
    case kSecImportItemTrustNotFound
}

public class MagicKeyShare {
    private static var share : MagicKeyShare?
    public private(set) var keychainGroupName : String?
    let keychainGroup : KeychainWrapper?
    
    init(keychainGroupName : String? = nil) throws {
        IDPLogger.info("Setting up MagicKeyShare")
        IDPLogger.info("Mode : Manual")
        self.keychainGroupName = keychainGroupName != nil ? keychainGroupName : MagicKeyShare.getKeychainGroupName()
        if ((self.keychainGroupName) != nil){
            self.keychainGroup = KeychainWrapper(serviceName : "keychainGroup", accessGroup: self.keychainGroupName)
        } else {
            IDPLogger.error("No Keychain Group Found")
            throw MagicKeyShareError.NoKeychainGroupFound
        }
        MagicKeyShare.share = self
    }

    class public func shared() throws -> MagicKeyShare!  {
        
        if(MagicKeyShare.share == nil) {
            do {
                MagicKeyShare.share = try MagicKeyShare()
            } catch let error {
                throw MagicKeyShareError.initFail(withError : error)
            }
        }
        return MagicKeyShare.share!
    }
     
    // gets the keychain group name from the app signature
    static public func getKeychainGroupName() -> String?  {
        let keychainGroupKey = Entitlements.Key("keychain-access-groups")
        
        if let keychainGroups = (UIApplication.shared.entitlements.value(forKey: keychainGroupKey) as! NSArray?) {
            if(keychainGroups.count > 0) {
                return "\(keychainGroups[0])"
            }
        }
        return nil
    }
    
    public func addKeyValueToKeyChain(keyName: String, value: String) -> Bool {
        let isSuccessful: Bool = keychainGroup!.set(value, forKey: keyName)
        IDPLogger.debug("Adding key to keychain")
        IDPLogger.debug("Success: \(isSuccessful)")
        return isSuccessful;
    }

    public func returnKeyFromKeychain(keyName: String) -> String {
        IDPLogger.info("Returning key from keychain with value: \(keyName)")
        let retrievedString: String? = keychainGroup!.string(forKey: keyName)
        return retrievedString ?? "Not Found";
    }
    
    public func deleteKeyFromKeychain(keyName: String) -> Bool {
        IDPLogger.info("Deleting key from keychain with value: \(keyName)")
        return keychainGroup!.removeObject(forKey: keyName)
    }
    
    /**
     Checks if keyName value is set on keyChain.
     */
    public func hasKeyOnKeychain(keyName: String) -> Bool {
        if let hasKey = keychainGroup?.hasValue(forKey: keyName) {
            return hasKey
        }

        return false
    }
}
