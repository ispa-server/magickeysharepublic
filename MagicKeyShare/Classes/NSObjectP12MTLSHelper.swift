import Foundation

@objc public class NSObjectP12MtlsHelper : NSObject{
    private static let share : NSObjectP12MtlsHelper = NSObjectP12MtlsHelper()
    var p12helper : P12MtlsHelper?
    
    @objc class public func shared() -> NSObjectP12MtlsHelper {
        return NSObjectP12MtlsHelper.share
    }
    
    override init() {
        do {
            self.p12helper = try P12MtlsHelper.shared()
        } catch let error {
            print(error)
        }
        super.init()
    }
    
    @objc public func getMainCert() -> NSString {
        if(p12helper == nil) {
            return OSStatus(errSecNotAvailable).description as NSString
        }
        do {
            return try p12helper!.getMainCert().description as NSString
        } catch let error {
            return error.localizedDescription as NSString
        }
    }
    
    @objc public func getP12FromKeychainGroup() -> NSString {
        if(p12helper == nil) {
            return "P12MtlsHelper init failed"
        }
        return p12helper!.getP12FromKeychainGroup() as NSString
    }
    
    @objc public func willHaveExpiredIn(seconds : Double) -> Bool {
        do {
            return try p12helper!.willHaveExpiredIn(seconds : seconds)
        } catch {
            print("P12MtlsHelper willHaveExpiredIn Error :\(error)")
            return true
        }
    }
    
    @objc public func getUserCertificateRenewalDate() -> Date? {
        do {
            return try p12helper!.getUserCertificateRenewalDate()
        } catch {
            print("P12MtlsHelper getUserCertificateRenewalDate Error :\(error)")
            return nil
        }
    }
    
    @objc public func deleteP12() -> Bool {
        if(p12helper == nil) {
            return false
        }
        return p12helper!.deleteP12()
    }

    @objc public func getP12Size() -> Int {
        if(p12helper == nil) {
            return 0
        }
        return p12helper!.p12Size
    }
    
    @objc public func getSidentity1() -> SecIdentity? {
        return p12helper!.sidentity1
    }
    
}
