import Foundation
import UIKit
import WebKit
import CoreFoundation

enum webViewError: Error {
    case OSStatus(OSStatus : Int32)
    case missingCertificate
    case authTypeMismatch(expectedType : String)
    case kSecImportItemCertChainNotFound
    case kSecImportItemTrustNotFound
}

open class MutualTlsViewController: UIViewController, WKUIDelegate, WKNavigationDelegate {
    public var configuration : WKWebViewConfiguration?
    private var kc : P12MtlsHelper? = nil
    public var url : URL?
    @IBOutlet var webView: WKWebView!

    open func loadWebview() {
        if(url != nil) {
            logger.debug("myURL = \(url!.absoluteString)")
            let myRequest = URLRequest(url: url!)
            startSpinner()
            webView.load(myRequest)
        }
    }

    open override func loadView() {
        if(configuration == nil) {
            configuration = WKWebViewConfiguration()
        }
        webView = WKWebView(frame: .zero, configuration: configuration!)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        view = webView
    }

    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        stopSpinner()
    }

    // This method should be overriden when the this class is subclassed
    open override func viewDidAppear(_ animated: Bool) {
        fatalError("Must Override viewWillAppear: Method should call loadWebview() or call your own setup code")
    }

    open func webView(_ webView: WKWebView,
           didReceive challenge: URLAuthenticationChallenge,
           completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void
    ) {
        do {
            // Deal with First server challenge
            if (challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust) {
                completionHandler(URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust:challenge.protectionSpace.serverTrust!))
                return
            }
            // Check Server wants AuthType: Certificate
            if(challenge.protectionSpace.authenticationMethod != NSURLAuthenticationMethodClientCertificate) {
                throw webViewError.authTypeMismatch(expectedType :challenge.protectionSpace.authenticationMethod)
            }
            // Certificate Auth
            logger.debug("AuthType: Certificate (\(NSURLAuthenticationMethodClientCertificate)")

            kc = try P12MtlsHelper.shared()

            let err = try kc!.getMainCert()

            if(err != errSecSuccess) {
                throw webViewError.OSStatus(OSStatus:err)
            }

            if(kc!.sidentity1 == nil) {
                throw webViewError.missingCertificate
            }

            let credential = URLCredential(identity: kc!.sidentity1!, certificates: nil, persistence: .forSession)
            completionHandler(.useCredential, credential)

        } catch webViewError.OSStatus(let OSStatus) {

            completionHandler(URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil)
            logger.error("Certificate OSStatus error: \(OSStatus.description)")
            ShowError(title : "Error", message : "Certificate OSStatus error: \(OSStatus.description)")

        } catch webViewError.authTypeMismatch(let expectedType) {

            completionHandler(URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil)
            logger.error("Server did not expect certificate Authentication\nExpected : \(expectedType)")
            ShowError(title : "Incorrect Auth Type", message : "Server did not expect certificate Authentication\nExpected : \(expectedType)")

        } catch let error {
            completionHandler(URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil)
            logger.error("Unexpected error: \(error)")
            ShowError(title : "Error", message : "Unexpected error: \(error)")
        }
    }

    public func ShowError(title : String, message : String) {
        // Create a new alert
        self.showError(
            withTitle: title,
            withSubtitle: message
        )
    }
}
