import Foundation
import IDPClientSDK

public enum P12MtlsHelperError: Error {
    case initFail(withError : Error)
    case OSStatus(OSStatus : Int32)
    case DecodeBase64StringFailed
    case GetSharedKeychainStringFailed
    case p12NotFound
    case p12isEmpty
    case SecPKCS12Import(withOSStatusError : Int32)
    case identityDictionariesNotFoundInP12
    case sidentity1NotFound
    case kSecImportItemCertChainNotFound
    case kSecImportItemTrustNotFound
}

extension String {

    /// Create `Data` from hexadecimal string representation
    ///
    /// This creates a `Data` object from hex string. Note, if the string has any spaces or non-hex characters (e.g. starts with '<' and with a '>'), those are ignored and only hex characters are processed.
    ///
    /// - returns: Data represented by this hexadecimal string.

    var hexadecimal: Data? {
        var data = Data(capacity: count / 2)

        let regex = try! NSRegularExpression(pattern: "[0-9a-f]{1,2}", options: .caseInsensitive)
        regex.enumerateMatches(in: self, range: NSRange(startIndex..., in: self)) { match, _, _ in
            let byteString = (self as NSString).substring(with: match!.range)
            let num = UInt8(byteString, radix: 16)!
            data.append(num)
        }

        guard data.count > 0 else { return nil }

        return data
    }
}

public class P12MtlsHelper : IDPLogger {
    public private(set) var sidentity1: SecIdentity?
    public private(set) var scert : SecCertificate?
    public private(set) var p12Size : Int = 0
    public private(set) var secTrust : SecTrust?
    public private(set) var identities: CFTypeRef?
    private let keychain : MagicKeyShare
    private static var share : P12MtlsHelper?

    init() throws {
        do {
            keychain = try MagicKeyShare.shared()
        } catch let MagicKeyShareError.initFail(withError: error) {
            throw P12MtlsHelperError.initFail(withError : error)
        }
    }

    class public func shared() throws -> P12MtlsHelper!  {

        if(P12MtlsHelper.share == nil) {
            do {
                P12MtlsHelper.share = try P12MtlsHelper()
            } catch let error {
                throw P12MtlsHelperError.initFail(withError : error)
            }
        }
        return P12MtlsHelper.share!
    }

    public func getP12FromKeychainGroup() -> String {
        do {
            let err = try self.getMainCert()

            if(err != errSecSuccess) {
                return "Certificate error:\(err)"
            }

        } catch P12MtlsHelperError.OSStatus(let OSStatus) {
            return "Certificate OSStatus error: \(OSStatus.description)"
        } catch let error {
            return "Certificate error : \(error.localizedDescription)"
        }
        if(scert == nil) {
            return "Certificate Not Found"
        }
        return "OK"
    }

    public func getMainCert() throws -> OSStatus  {
        guard let identity = keychain.returnKeyFromKeychain(keyName: "ersaData1") as String? else {
            throw P12MtlsHelperError.GetSharedKeychainStringFailed
        }

        if(identity == "Not Found") {
            throw P12MtlsHelperError.p12NotFound
        }

        if(identity.isEmpty) {
            throw P12MtlsHelperError.p12isEmpty
        }

        let dataRef = identity.hexadecimal!

        let key : NSString = kSecImportExportPassphrase as NSString
        let options : NSDictionary = [key : ""]
        var importResult: CFArray? = nil
        let oSStatus = SecPKCS12Import(
            dataRef as NSData,
            options,
            &importResult)

        var identityDictionaries : [[String:Any]] = []

        print("err == errSecSuccess = \(oSStatus) = \(errSecSuccess)")
        if (oSStatus == errSecSuccess) {
            identityDictionaries = importResult as! [[String:Any]]
        } else {
            throw P12MtlsHelperError.SecPKCS12Import(withOSStatusError: oSStatus)
        }
        if(identityDictionaries.isEmpty) {
            throw P12MtlsHelperError.identityDictionariesNotFoundInP12
        }
        let first = identityDictionaries[0]

        if((first[kSecImportItemIdentity as String] ) == nil) {
            throw P12MtlsHelperError.sidentity1NotFound
        }
        self.sidentity1 = (first[kSecImportItemIdentity as String] as! SecIdentity)

        if((first[kSecImportItemCertChain as String] ) == nil) {
            throw P12MtlsHelperError.kSecImportItemCertChainNotFound
        }
        self.scert = (first[kSecImportItemCertChain as String] as! SecCertificate)

        if((first[kSecImportItemTrust as String] ) == nil) {
            throw P12MtlsHelperError.kSecImportItemTrustNotFound
        }
        self.secTrust = (first[kSecImportItemTrust as String] as! SecTrust)

        logger.debug("scert : \(String(describing: self.scert))")
        logger.debug("secTrust : \(String(describing: self.secTrust))")

        for key in first {
            logger.debug("keyvalue = \(key)")
        }

        p12Size = identity.count

        return oSStatus
    }

    public func getUserCertificateRenewalDate() throws -> Date? {

        // If user has this key, run the migration and convert it to the new name "userCertificateRenewalDate"
        if (keychain.hasKeyOnKeychain(keyName: "ephemeralCertRenewalDate")) {
            let renewalDate = keychain.returnKeyFromKeychain(keyName: "ephemeralCertRenewalDate")
            let _ = keychain.addKeyValueToKeyChain(keyName: "userCertificateRenewalDate", value: renewalDate)
            let _ = keychain.deleteKeyFromKeychain(keyName: "ephemeralCertRenewalDate")
        }

        guard let dateString = keychain.returnKeyFromKeychain(keyName: "userCertificateRenewalDate") as String? else {
            throw P12MtlsHelperError.GetSharedKeychainStringFailed
        }

        if (dateString == "Not Found") {
            return nil
        }
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_GB") // set locale to reliable en_GB
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let userCertificateRenewalDate = dateFormatter.date(from:dateString)!
        return userCertificateRenewalDate
    }

    public func deleteP12() -> Bool {
        if (keychain.hasKeyOnKeychain(keyName: "ephemeralCertRenewalDate")) {
            if(keychain.deleteKeyFromKeychain(keyName: "ephemeralCertRenewalDate")) {
                if(keychain.deleteKeyFromKeychain(keyName: "ersaData1")) {
                    return true
                }
            }
        }
        if (keychain.hasKeyOnKeychain(keyName: "userCertificateRenewalDate")) {
            if(keychain.deleteKeyFromKeychain(keyName: "userCertificateRenewalDate")) {
                if(keychain.deleteKeyFromKeychain(keyName: "ersaData1")) {
                    return true
                }
            }
        }
        return false
    }

    public func willHaveExpiredIn(seconds : Double) throws -> Bool {
        let renewalDate = try getUserCertificateRenewalDate()
        if(renewalDate != nil) {
            let timeInFuture = Date().addingTimeInterval(seconds)
            return (timeInFuture > renewalDate!)
        }
        return true
    }

    public func getUserCertTimeToExpire() -> String {
        do {
            let renewalDate = try getUserCertificateRenewalDate()
            if(renewalDate != nil) {
                let now = Date()
                if(renewalDate! < now) {
                    logger.debug("User certificate has expired")
                    return "Expired"
                }
                //here we change the seconds to hours,minutes and days

                let CompetitionDayDifference = Calendar.current.dateComponents([.day, .hour, .minute, .second], from: Date(), to: renewalDate!)

                //finally, here we set the variable to our remaining time
                let daysLeft = CompetitionDayDifference.day
                let hoursLeft = CompetitionDayDifference.hour
                let minutesLeft = CompetitionDayDifference.minute
                let secondsLeft = CompetitionDayDifference.second
                var timeLeft = ""
                if(daysLeft! > 0) {
                    timeLeft += "\(daysLeft ?? 0) Days "
                }
                timeLeft += "\(hoursLeft ?? 0)h "
                timeLeft += "\(minutesLeft ?? 0)m "
                timeLeft += "\(secondsLeft ?? 0)s"
                return timeLeft
            }
        } catch let error {
            logger.error("Failed to get user certificate time to expire :\(error)")
            return "Failed to get user certificate time to expire"
        }
        return "Unknown"
    }

    func getCertInfo() -> String {
        if(scert == nil) {
            logger.error("Certificate Not Found")
            return "Certificate Not Found"
        }
        let certificateSummary = (SecCertificateCopySubjectSummary(scert!) ?? "Not Readable" as CFString)
        return "\(certificateSummary)"
    }

    public func loadDemoP12intoKeyChain(demoP12 : String, demoP12ExpireDate : String) -> Bool {
        if(
            !demoP12.isEmpty &&
            keychain.addKeyValueToKeyChain(keyName: "ersaData1", value: demoP12) &&

            keychain.addKeyValueToKeyChain(keyName: "userCertificateRenewalDate", value: demoP12ExpireDate)
        ) {
            return true
        }
        logger.warn("Could not find the demo12, have you asked idenprotect for it yet?")
        return false
    }

}
