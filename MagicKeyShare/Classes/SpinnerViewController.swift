//
//  SpinnerViewController.swift
//  MagicKeyShare_ExampleA
//
//  Created by James Trigg on 06/05/2022.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    public func startSpinner() {
        DispatchQueue.main.async { [self] in
            let spinner = SpinnerViewController()
            
            // add the spinner view controller
            self.addChild(spinner)
            spinner.view.frame = view.frame
            view.addSubview(spinner.view)
            spinner.didMove(toParent: self)
        }
    }
    
    public func stopSpinner() {
        DispatchQueue.main.async {
            // find and remove any spinner view controller
            let spinners = self.children.filter{$0 is SpinnerViewController} as! [SpinnerViewController]
            for spinner in spinners {
                spinner.willMove(toParent: nil)
                spinner.view.removeFromSuperview()
                spinner.removeFromParent()
            }
        }
    }
}

class SpinnerViewController: UIViewController {
    var spinner = UIActivityIndicatorView(style: .large)

    override func loadView() {
        view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.7)
        spinner.color = UIColor.white
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.startAnimating()
        view.addSubview(spinner)

        spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
}
