//
//  IDPNetwork+KeyChain.swift
//  MagicKeyShare
//
//  Created by James Trigg on 05/05/2022.
//

import Foundation
import IDPClientSDK

let demo_ca_certs = """
-----BEGIN CERTIFICATE-----
MIIDfzCCAmegAwIBAgIQVduTj7dsKY9KSShF1g1iJzANBgkqhkiG9w0BAQsFADBS
MRMwEQYKCZImiZPyLGQBGRYDbmV0MRcwFQYKCZImiZPyLGQBGRYHaWRlbjMxOTES
MBAGCgmSJomT8ixkARkWAmFkMQ4wDAYDVQQDEwVDQTAwMTAeFw0yMTA5MjgxNzI5
NTJaFw0yNjA5MjgxNzM5NDlaMFIxEzARBgoJkiaJk/IsZAEZFgNuZXQxFzAVBgoJ
kiaJk/IsZAEZFgdpZGVuMzE5MRIwEAYKCZImiZPyLGQBGRYCYWQxDjAMBgNVBAMT
BUNBMDAxMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwn3eJCquUSUB
K2tnOM4irSB39VYwqQb3Q0AK/7yxSroj9toeV3pQT5BvCMLCSGvghB0HrvaE/+6l
dXVa5+dxjxk9jF5AV23C0TB5hPuE6mn7tDUV5WUKGpBbd8di5aSOFGkgst/QS9BZ
UywSz1KvWpfD/oWFMXx3u/0FoMRhfgHyW3+6dQPWQaXCckkjgaVNy4vO1dwBa6vR
CdxdpEf9CZbFsmWFBQGloEUjOs1xSVX4sP2LluVinpiikypgPfZ0Y4/jKloRYvAr
pQe2F+IIV6slydzgO+r5NjVgdp6R2ImujhdNNqE9ikGwS/SvGbZuz/bMlul0OzYu
SeEIOWTrDQIDAQABo1EwTzALBgNVHQ8EBAMCAYYwDwYDVR0TAQH/BAUwAwEB/zAd
BgNVHQ4EFgQUYofVJDwRkyZDUZ2h2ywRnbVms+IwEAYJKwYBBAGCNxUBBAMCAQAw
DQYJKoZIhvcNAQELBQADggEBAE5F3Q/El/a8sbwSlQvLOyAFaVkU1NAK15qRn40n
DhBCbaY2hipBMntAfUJB0NOwCDVbrKe2qJIcVx4RPLTQTmpYM7MSlZY9yl/qk2Fa
inXo3Bag7yFxSHkvdwqlPrZwk1hnEg13ypRrzdkX80xAaE5fI1lFyE+V3f7yt/5z
FvJAFU3Z6fP2to9TX2RYGE79guL5nmkkv60+12PD9VJ0q/QIPGbKWZYuDPLh+Pvm
Rw4TzWZK25Q/nQFyGFN2y1EmDppHR7qnQam2W6TOMOjt9kQC8c29Jc8wN4N7+71A
td37AnIQlclbhrllsIwES69xGCBWUVszb4KNU6YTnpEaOg0=
-----END CERTIFICATE-----
-----BEGIN CERTIFICATE-----
MIIGYzCCBUugAwIBAgITGgAAA2rL7APc66EmVgAAAAADajANBgkqhkiG9w0BAQsF
ADBSMRMwEQYKCZImiZPyLGQBGRYDbmV0MRcwFQYKCZImiZPyLGQBGRYHaWRlbjMx
OTESMBAGCgmSJomT8ixkARkWAmFkMQ4wDAYDVQQDEwVDQTAwMTAeFw0yMjAyMjIx
MDI3MjhaFw0yNDAyMjIxMDM3MjhaMGYxEzARBgoJkiaJk/IsZAEZFgNuZXQxFzAV
BgoJkiaJk/IsZAEZFgdpZGVuMzE5MRIwEAYKCZImiZPyLGQBGRYCYWQxEjAQBgoJ
kiaJk/IsZAEZFgJhYjEOMAwGA1UEAxMFQ0EwMDIwggIiMA0GCSqGSIb3DQEBAQUA
A4ICDwAwggIKAoICAQDTRnbs+G2wLuHa/UEqjlVJrsfmNCQURdH0d88l0Xh5uMbu
5aeYQLdjdtUfed3gGCaUdgDEbIY5jx0jrrnq4f0ESHOxgcpl4r8EYLu6z/ia3H3O
hwxin3DtlQPafgpcjAT4KSt4AcihE2W664KELt4sJvZATh8n6a/NCl6scKegUSA8
e3wJXz9NnOgEnvbpK+18241K7CcpIy8JzcUKP+cLNmrp4Zol7sktxzaFovpaH1I4
eOCBwUW42E7L2wzqHz1uo+7E/aUcUOc6XCzCCNI5Zy6h4cRZtt1nebhenRrQW0H5
ZiWLUHquNf+oowWWDpTkodpbv1hs3fR9gjrKkXrOncrHl1eUpUdsQxIKD9yBtFcq
5paYfiSvSSpfMRmcwmMoag4lt+CBQTKy7+9iv8iQQLQoSuU84tno+Zv5jcY9G0yV
VbszkDOSEPnHA1T7AAXZQZrKcJ9E7nGDrRtjZWbZtfan5uIHLfIN22hE8fLYrFns
ALj33PhgODOWtCCmX7Uv9K3rFcVeKR7BnOkFSddKlAsWcggR/uSSwlbGOX3Jzuln
aYM7zMstq8og4TgKKCvDUQ8I6CMVl5Vx4E6T8p1U3rv3sepWUu12S1TmdiDtrInB
XQQqTatPaw/WaA9ygLXXn63WXKB/Ct5kSrrNil8rgl/zRpFDfAgZlZZxw+2lnQID
AQABo4ICHDCCAhgwEAYJKwYBBAGCNxUBBAMCAQAwHQYDVR0OBBYEFFdJA+cUbjlc
fzV7l0X1uijU6sm7MBkGCSsGAQQBgjcUAgQMHgoAUwB1AGIAQwBBMA4GA1UdDwEB
/wQEAwIBhjAPBgNVHRMBAf8EBTADAQH/MB8GA1UdIwQYMBaAFGKH1SQ8EZMmQ1Gd
odssEZ21ZrPiMIHHBgNVHR8Egb8wgbwwgbmggbaggbOGgbBsZGFwOi8vL0NOPUNB
MDAxLENOPURDMDAxLENOPUNEUCxDTj1QdWJsaWMlMjBLZXklMjBTZXJ2aWNlcyxD
Tj1TZXJ2aWNlcyxDTj1Db25maWd1cmF0aW9uLERDPWFkLERDPWlkZW4zMTksREM9
bmV0P2NlcnRpZmljYXRlUmV2b2NhdGlvbkxpc3Q/YmFzZT9vYmplY3RDbGFzcz1j
UkxEaXN0cmlidXRpb25Qb2ludDCBvQYIKwYBBQUHAQEEgbAwga0wgaoGCCsGAQUF
BzAChoGdbGRhcDovLy9DTj1DQTAwMSxDTj1BSUEsQ049UHVibGljJTIwS2V5JTIw
U2VydmljZXMsQ049U2VydmljZXMsQ049Q29uZmlndXJhdGlvbixEQz1hZCxEQz1p
ZGVuMzE5LERDPW5ldD9jQUNlcnRpZmljYXRlP2Jhc2U/b2JqZWN0Q2xhc3M9Y2Vy
dGlmaWNhdGlvbkF1dGhvcml0eTANBgkqhkiG9w0BAQsFAAOCAQEATPkO/gYi0Wzn
ypbf5sHM8GYkuV2GOo52n+cRVGguB36G0BoBTaraLv7mI22kkXsfP09KFsWPRMDX
egiibxt4tSdKVyScd5VM4VCoAvJnrVCAiDRkbgq8BDJ/US3UChH2aPC4sHfS8Vsc
4rBKhGbogRtEIATHX2iK5QKeSTdyDmfaNYCh8I/GqAd9TAtkVkW7oNBDAnvTqWVQ
BkzAHod+t7AAhM7/nlnBDGPQ2vgPkH77szg7C8oHpuzmTPS59WHBDqZ7uFAljdBH
6+nKhA7s+Fcdz0A3cBogZCDAFaTm5gnTpRjnE+PmRAQdk91fTe4beJEpNw1/iAtp
Zw4+KEsIBw==
-----END CERTIFICATE-----
"""

extension IDPNetwork {
    public func RefreshCertificateStoreFromKeyChain() {
        var certs : [SecCertificate] = []
        let certList = demo_ca_certs.components(separatedBy: "-----END CERTIFICATE-----")
        for certString in certList {
            if(certString.isEmpty) {
                continue
            }
            let b64 = certString
                .replacingOccurrences(of: "-----BEGIN CERTIFICATE-----", with: "")
                .replacingOccurrences(of: "\r", with: "")
                .replacingOccurrences(of: "\n", with: "")

            let data = (Data(base64Encoded: b64) ?? Data()) as NSData
            let cert = SecCertificateCreateWithData(kCFAllocatorDefault, data)
            IDPLogger.info("RefreshCertificateStore :: cert => \(String(describing: cert))" )
            if let cert = cert {
                certs.append(cert)
            }
        }
        self.RefreshCertificateStore(certs)
        
    }
}
