//
//  IDPLogger.swift
//  CocoaAsyncSocket
//
//  Created by James Trigg on 08/04/2022.
//

import Foundation

protocol IDPLoggableDelegate {
    func trace(
        file: String,
        line : Int,
        location : String,
        fn : String,
        _ text : String
    )
    
    func debug(
        file: String,
        line : Int,
        location : String,
        fn : String,
        _ text : String
    )
    
    func info(
        file: String,
        line : Int,
        location : String,
        fn : String,
        _ text : String
    )
    
    func warn(
        file: String,
        line : Int,
        location : String,
        fn : String,
        _ text : String
    )
    
    func error(
        file: String,
        line : Int,
        location : String,
        fn : String,
        _ text : String
    )
    
    func fatal(
        file: String,
        line : Int,
        location : String,
        fn : String,
        _ text : String
    )

}
