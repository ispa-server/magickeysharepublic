//
//  IDPClientSDK.h
//  IDPClientSDK
//
//  Created by James Trigg on 12/04/2022.
//

#import <Foundation/Foundation.h>

//! Project version number for IDPClientSDK.
FOUNDATION_EXPORT double IDPClientSDKVersionNumber;

//! Project version string for IDPClientSDK.
FOUNDATION_EXPORT const unsigned char IDPClientSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <IDPClientSDK/PublicHeader.h>
