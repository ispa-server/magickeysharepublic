Pod::Spec.new do |s|
  s.name             = 'MagicKeyShare'
  s.version          = '3.0.4'
  s.summary          = 'Sharing P12 between apps for MTLS.'
  s.description      =
  'Share keys between apps by the same developer So other apps can do MTLS auth with keys created by an other app'
  s.homepage         = 'https://bitbucket.org/ispa-server/magickeyshare'
  s.author           = { 'James Trigg' => 'james.trigg@idenprotect.com' }
  s.source           = { :git => 'https://jltiDENprotect@bitbucket.org/ispa-server/magickeysharepublic.git', :tag => s.version.to_s }
  s.platform     = :ios, '14.7'
  s.ios.deployment_target = '14.7'
  s.vendored_frameworks = 'MagicKeyShare/Frameworks/IDPClientSDK.xcframework'
  s.source_files = 'MagicKeyShare/Classes/**/*'
  s.dependency 'SwiftKeychainWrapper'
  s.dependency 'SwiftEntitlements'
  s.dependency 'FCAlertView'
  s.dependency 'WRUserSettings'
end
